import java.util.*;

/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    /**
     * Main method. Different tests.
     */
    public static void main(String[] param) {

        System.out.println(valueOf("2/2/"));
    }

    private long numerator;
    private long denominator;

    /**
     * Constructor.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        this.numerator = a;
        this.denominator = b;
        if (denominator == 0) {
            throw new RuntimeException("Denominator can't be equals to zero!");
        }
        this.numerator = a/findCommonDivider(a,b);
        this.denominator = b/findCommonDivider(a,b);
        if (denominator < 0){
            denominator = Math.abs(denominator);
            numerator = -numerator;
        }
        if (numerator == 0) {
            denominator = 1;
        }

    }

    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {
        return numerator;
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {
        return denominator;
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        return String.format("%s/%s", numerator, denominator);
    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
        return this.compareTo((Lfraction) m) == 0;
    }

    /**
     * Hashcode has to be equal for equal fractions.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    /**
     * Sum of fractions.
     *
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {
        long newNumerator;
        long newDenominator;
        if (denominator == m.denominator) {
            newNumerator = numerator + m.numerator;
            newDenominator = denominator;
        } else {
            newDenominator = denominator * m.denominator;
            newNumerator = (newDenominator / denominator) * numerator + (newDenominator / m.denominator) * m.numerator;
        }
        Lfraction result = new Lfraction(newNumerator, newDenominator);
        return simplifyFraction(result);
    }

    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        long newNumerator;
        long newDenominator;
        newNumerator = numerator * m.numerator;
        newDenominator = denominator * m.denominator;
        Lfraction result = new Lfraction(newNumerator, newDenominator);
        return simplifyFraction(result);
    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if (numerator == 0) {
            throw new RuntimeException("Denominator can't be equals to zero!");
        }

        long newDenominator = numerator;
        long newNumerator = denominator;
        if (numerator < 0) {
            newDenominator = 0 - numerator;
            newNumerator = -denominator;
        }

        return new Lfraction(newNumerator, newDenominator);
    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        return new Lfraction(-numerator, denominator);
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
        return this.plus(m.opposite());
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
        if (m.numerator == 0) {
            throw new RuntimeException("Division by zero exception!");
        }
        return this.times(m.inverse());
//        long newNumerator;
//        long newDenominator;
//        newNumerator = numerator * m.denominator;
//        newDenominator = denominator * m.numerator;
//
//        Lfraction result = new Lfraction(newNumerator, newDenominator);
//        return simplifyFraction(result);
    }

    /**
     * Comarision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        long a = this.numerator * m.denominator;
        long b = m.numerator * this.denominator;
        if (Long.compare(a, b) == -1) {
            return -1;
        } else if (a == b) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Clone of the fraction.
     *
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Lfraction(numerator, denominator);
    }

    /**
     * Integer part of the (improper) fraction.
     *
     * @return integer part of this fraction
     */
    public long integerPart() {
        if (Math.abs(numerator) < Math.abs(denominator)) {
            return 0L;
        } else if (Math.abs(numerator) == Math.abs(denominator)) {
            if (numerator < 0) {
                return -1L;
            }
            return 1L;
        } else if (numerator == 0) {
            return 0;
        } else {
            return numerator / denominator;
        }
    }

    /**
     * Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     *
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        if (Math.abs(numerator) < Math.abs(denominator)) {
            return new Lfraction(numerator, denominator);
        } else {
            long integer = this.integerPart();
            long newNumerator = numerator - denominator * integer;
            Lfraction result = new Lfraction(newNumerator, denominator);
            return simplifyFraction(result);
        }

    }

    /**
     * Approximate value of the fraction.
     *
     * @return numeric value of this fraction
     */
    public double toDouble() {
        return (double) numerator / denominator; // TODO!!!
    }

    /**
     * Double value f presented as a fraction with denominator d > 0.
     *
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {

        long longPart = (long) f;
        double doublePart = f - longPart;
        long newNumerator;
        if (doublePart == 0.0) {
            newNumerator = (d * longPart);
        } else {
            newNumerator = (d * longPart + Math.round(doublePart * d));
        }
        Lfraction result = new Lfraction(newNumerator, d);
        return simplifyFraction(result);
    }

    /**
     * Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     *
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {
        if (!s.contains("/")) {
            throw new RuntimeException("String value: " + s + " is not correct!");
        }
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '/'){
                count++;
            }
            if (Character.isLetter(s.charAt(i))) {
                throw new RuntimeException(String.format("Illegal symbol: %s in expression: %s", s.charAt(i), s));
            }
        }
        if (count > 1){
            throw new RuntimeException("String value: " + s + " is not correct!");
        }


        String[] values = s.split("/");

        if (values.length != 2) {
            throw new RuntimeException("String value: " + s + " is not correct!");
        }
        for (String value : values) {
            try {
                Long.valueOf(value.trim());
            } catch (NumberFormatException e) {
                throw new RuntimeException(String.format("Numeric value: %s in string: %s is not correct!", value, s));
            }
        }

        return new Lfraction(Long.parseLong(values[0]), Long.parseLong(values[1]));
    }

    public static Lfraction simplifyFraction(Lfraction fractionToSimplify) {

        long newNumerator;
        long newDenominator;

        long d;
        long numerator = fractionToSimplify.numerator;
        boolean lowerThanZero = false;
        if (numerator < 0) {
            numerator = -fractionToSimplify.numerator;
            lowerThanZero = true;
        }
        d = findCommonDivider(numerator, fractionToSimplify.denominator);

        newNumerator = numerator / d;
        newDenominator = fractionToSimplify.denominator / d;
        if (lowerThanZero) {
            newNumerator = -newNumerator;
        }

        return new Lfraction(newNumerator, newDenominator);

    }

    private static long findCommonDivider(long a, long b) {
        if (b == 0) {
            return Math.abs(a);
        }
        return findCommonDivider(b, a % b);
    }
}

